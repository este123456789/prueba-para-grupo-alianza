<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpleadoPorCargosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleado_por_cargos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('empleadoId');
            $table->unsignedBigInteger('cargoId');
            $table->timestamps();

            $table->foreign('empleadoId')->references('id')->on('empleados')->onDelete('cascade');
            $table->foreign('cargoId')->references('id')->on('cargos')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleado_por_cargos');
    }
}
