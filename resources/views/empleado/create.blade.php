@extends('layouts.app')
@section('content')


<form action="{{url('/empleado')}}" method="post">
    @csrf
    <div class="form-group">

        <label for="nombres">
            Nombres:
        </label>
        <input class="form-control" type="text" name="nombres">
        <label for="apellidos">
            Apellidos:
        </label>
        <input class="form-control" type="text" name="apellidos">
        <label for="identificacion">
            identificación
        </label>
        <input class="form-control" type="text" name="identificacion">
        <label for="direccion">
            Dirección:
        </label>
        <input class="form-control" type="text" name="direccion">
        <label for="telefono">
            Teléfono
        </label>
        <input class="form-control" type="text" name="telefono">
        <label for="pais">
            Pais
        </label>
        <input class="form-control" type="text" name="pais">
        <label for="cuidadDeNacimiento">
            Ciudad de nacimíento:
        </label>
        <select class="form-control" name="ciudadDeNacimiento" id="ciudadDeNacimiento">
            <option v-for="ciudad in datos" value="">@{{ciudad}}</option>
        </select>
        <label for="rol">
            Rol:
        </label>
        <select class="form-control" name="rol" id="rol">
            <option></option>
            <option value="jefe">Jefe</option>
            <option value="colaborador">Colaborador</option>
        </select>
        <div class="clearfix"></div>
        <input class="btn btn-success" type="submit" value="Guardar">
    </div>
</form>


@endsection