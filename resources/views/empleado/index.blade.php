
@extends('layouts.app')
@section('content')


<div class="clearfix"></div>
<a href="<?php echo url('/empleado/create'); ?>" class="btn btn-success">Crear Nuevo Empleado</a>
<div class="clearfix"></div>

<table class="table table-striped">

    <thead>
        <tr>
            <th>#</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Identificación</th>
            <th>Dirección</th>
            <th>Teléfono</th>
            <th>Pais</th>
            <th>Ciudad de Nacimiento</th>
            <th>Rol</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($empleados as $empleado)
        <tr>
            <td>{{ $empleado->id }}</td>
            <td>{{ $empleado->nombres }}</td>
            <td>{{ $empleado->apellidos }}</td>
            <td>{{ $empleado->identificacion }}</td>
            <td>{{ $empleado->direccion }}</td>
            <td>{{ $empleado->telefono }}</td>
            <td>{{ $empleado->pais }}</td>
            <td>{{ $empleado->ciudadDeNacimiento }}</td>
            <td>{{ $empleado->rol }}</td>
            <td class="button-row">
            <a class="btn btn-warning" href="{{ url('/empleado/'.$empleado->id.'/edit') }}">

                Editar
            </a>                

            <form action="{{url('/empleado/'.$empleado->id)}}" method="post">
                    @csrf
                    {{method_field('DELETE')}}
                    <input class="btn btn-danger" type="submit" value="Borrar" >
            </form>
            </td>
        </tr>
        @endforeach
    </tbody>




</table>

@endsection
