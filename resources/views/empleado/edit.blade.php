
@extends('layouts.app')
@section('content')



<form action="{{url('/empleado/'.$empleado->id)}}" method="post">
    <div class="form-group">
        @csrf
        {{method_field('PATCH')}}
        @include('empleado.form')
    </div>
</form>


@endsection
