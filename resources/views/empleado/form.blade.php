<label for="nombres">
    Nombres:
</label>
<input type="text" class="form-control" required="required" value="{{$empleado->nombres}}" name="nombres">
<label for="apellidos">
    Apellidos:
</label>
<input type="text" class="form-control" required="required" value="{{$empleado->apellidos}}" name="apellidos">
<label for="identificacion">
    identificación
</label>
<input type="text" class="form-control" required="required" value="{{$empleado->identificacion}}" name="identificacion">
<label for="direccion">
    Dirección:
</label>
<input type="text" class="form-control" required="required" value="{{$empleado->direccion}}" name="direccion">
<label for="telefono">
    Teléfono
</label>
<input type="text" class="form-control" required="required" value="{{$empleado->telefono}}" name="telefono">
<label for="pais">
    Pais
</label>
<input type="text" class="form-control" required="required" value="{{$empleado->pais}}" name="pais">
<label for="cuidadDeNacimiento">
    Ciudad de nacimíento:
</label>

<select class="form-control" name="ciudadDeNacimiento" id="ciudadDeNacimiento" required="required" >
            <option v-for="ciudad in datos" value="">@{{ciudad}}</option>
        </select>


<label for="rol">
    Rol:
    <select name="rol" id="rol" class="form-control" required="required"> 
        <option value="{{$empleado->rol}}">{{$empleado->rol}}</option>
        <option value="jefe">Jefe</option>
        <option value="colaborador">Colaborador</option>
    </select>
</label>




<div class="clearfix"></div>
<input type="submit" value="Guardar" class="btn btn-success">