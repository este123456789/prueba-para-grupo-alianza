@extends('layouts.app')
@section('content')

<div class="clearfix"></div>
<a href="<?php echo url('/cargo/create'); ?>" class="btn btn-success">Crear Nuevo Cargo</a>
<div class="clearfix"></div>

<table class="table table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Nombres</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($cargos as $cargo)
        <tr>
            <td>{{ $cargo->id }}</td>
            <td>{{ $cargo->Nombre }}</td>
            <td class="button-row">
            <a class="btn btn-warning" href="{{ url('/cargo/'.$cargo->id.'/edit') }}">

                Editar
            </a>                

            <form action="{{url('/cargo/'.$cargo->id)}}" method="post">
                    @csrf
                    {{method_field('DELETE')}}
                    <input class="btn btn-danger" type="submit" value="Borrar" >
            </form>
            </td>
        </tr>
        @endforeach
    </tbody>




</table>


@endsection