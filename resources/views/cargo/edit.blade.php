@extends('layouts.app')
@section('content')

<form action="{{url('/cargo/'.$cargo->id)}}" method="post">
<div class="form-group">

    @csrf
    {{method_field('PATCH')}}
    @include('cargo.form')
</div>
</form>

@endsection