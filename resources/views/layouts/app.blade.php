<!doctype html>
<html>
<head>
   @include('includes.head')
</head>
<body>
<div class="container" >
    

<header class="row">
    @include('includes.header')
</header>
<div id="app" class="container-fluid cont">
    @yield('content')
</div>
   <footer class="row">
       @include('includes.footer')
   </footer>
</div>

<script>

new Vue({
  el: '#app',
  data () {
    return {
      datos: null
    }
  },
  mounted () {
    axios
      .get('<?php echo url('/json/full_city_list.json'); ?>')
      .then(
          response => (
            this.datos = response.data
          )

      )
  }
})

    </script>


</body>
</html>