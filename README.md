# Prueba para Grupo Alianza
Realizado en  :

-   [Laravel 8](https://laravel.com/)
-   MySql
-   [Vue 2.5.16 ](https://vuejs.org/)
-   [Axios] https://axios-http.com/docs/intro

## Urls 🚀


http://localhost/empleadosapp/public/empleado

http://localhost/empleadosapp/public/cargo



# Versionado 📌
»
«
###
1.0.0


# Prerequisitos 📌
»
«
###

-  [Xampp](recomendado)(https://www.apachefriends.org/es/index.html).
-  [Composer](https://getcomposer.org/).
-  [Mysql](https://dev.mysql.com/downloads/installer/).


### Instalaciòn 📋

- Instalar Xampp o entorno con php 7 o superiores, apache o ngnix, mysql.
- Crear carpeta que contenga el proyecto en la ubicación /xampp/htdocs 
- Crear base de datos en http://localhost/phpmyadmin
- Configurar el archivo .env  del proyecto con los datos de conexion de la base de datos. para mayor información consultar el enlace https://laravel.com/docs/9.x/configurationgit sta
- Ubicarse por terminal o cmd en la carpeta del proyecto y correr comando:  php artisan migrate


## Autor ✒️

Esteban Villa Ramirez. 2022

